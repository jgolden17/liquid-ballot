import { Query, Resolver } from '@nestjs/graphql';
import { ContestStatus, ContestType } from 'src/common';
import { Contest } from '../types/contests/contest.type';

@Resolver(() => Contest)
export class ContestResolver {
  @Query(() => Contest)
  async contest(): Promise<Contest> {
    return {
      id: '1234',
      type: ContestType.Plurality,
      title: 'Foo bar',
      slug: 'foo-bar',
      body: 'Foo bar',
      choices: ['Foo', 'Bar'],
      policy: 'Technology and Services',
      status: ContestStatus.Draft,
    };
  }
}
