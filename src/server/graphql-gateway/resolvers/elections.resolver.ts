import { Logger } from '@nestjs/common';
import { Args, Mutation, Query, Resolver } from '@nestjs/graphql';
import { ContestStatus, ContestType, ElectionStatus } from 'src/common';
import { ElectionsService } from 'src/server/elections/elections.service';
import { AddContestInput } from '../types/elections/add-contest.input';
import { CreateElectionInput } from '../types/elections/create-election.input';
import {
  AddContestResponse,
  CreateElectionResponse,
  Election,
  ElectionsConnection,
} from '../types/elections/election.type';

const contests = [
  {
    id: '1234',
    title: 'Branch Secretary Treasurer',
    type: ContestType.RankedSimple,
    body: 'Who should serve as the Branch Secretary Treasurer for the year of 2023',
    choices: ['Muddy', 'Ringo', 'Kona', 'Peanut'],
    status: ContestStatus.Draft,
    policy: 'Branch Administration',
    slug: 'branch-secretary-treasurer',
  },
  {
    id: '12345',
    title: 'Technology Committee Chair',
    type: ContestType.RankedSimple,
    body: 'Who should serve as the Branch Secretary Treasurer for the year of 2023',
    choices: ['Muddy', 'Ringo', 'Kona', 'Peanut'],
    status: ContestStatus.Draft,
    policy: 'Branch Administration',
    slug: 'branch-secretary-treasurer',
  },
];

@Resolver(() => Election)
export class ElectionResolver {
  private readonly logger = new Logger(ElectionResolver.name);

  constructor(private readonly service: ElectionsService) {}

  @Query(() => Election)
  async election(@Args('slug') slug: string): Promise<Election> {
    return {
      id: '1324',
      title: '2022 General Election and Referendum',
      contests,
      slug,
      status: ElectionStatus.Draft,
      notes: '',
      description: '',
      startDate: new Date(),
      endDate: new Date(),
      createdAt: new Date(),
    };
  }

  @Query(() => ElectionsConnection)
  async elections(): Promise<ElectionsConnection> {
    const { results, totalCount } = await this.service.getElections();

    const nodes = results.map((result) => new Election(result));

    return {
      nodes,
      edges: nodes.map((node) => ({ cursor: node.id, node })),
      totalCount,
      hasNextPage: false,
    };
  }

  @Mutation(() => CreateElectionResponse)
  async createElection(@Args('input') input: CreateElectionInput) {
    await this.service.create(input);

    return new CreateElectionResponse('Election created');
  }

  @Mutation(() => AddContestResponse)
  async addContest(@Args('input') input: AddContestInput) {
    await this.service.addContest(input.electionId, input.contestId);

    return new AddContestResponse('Contest added');
  }
}
