import { Field, ObjectType } from '@nestjs/graphql';
import { IElection, IContest, ElectionStatus } from 'src/common';
import { Connection } from 'src/server/graphql-gateway/types/connection';
import { Contest } from '../contests/contest.type';

@ObjectType()
export class Election implements IElection {
  @Field()
  notes: string;

  @Field()
  description: string;

  @Field()
  startDate: Date;

  @Field()
  endDate: Date;

  @Field()
  createdAt: Date;

  @Field()
  slug: string;

  @Field()
  status: ElectionStatus;

  @Field()
  title: string;

  @Field()
  id: string;

  @Field(() => [Contest])
  contests: IContest[];

  constructor(props: Partial<IElection>) {
    this.notes = props.notes;
    this.description = props.description;
    this.startDate = props.startDate;
    this.endDate = props.endDate;
    this.createdAt = props.createdAt;
    this.slug = props.slug;
    this.status = props.status;
    this.title = props.title;
    this.contests = props.contests;
    this.id = props.id;
  }
}

@ObjectType()
export class ElectionsConnection extends Connection(Election) {}

@ObjectType()
export class AddContestResponse {
  @Field()
  message: string;

  constructor(message: string) {
    this.message = message;
  }
}

@ObjectType()
export class CreateElectionResponse {
  @Field()
  message: string;

  constructor(message: string) {
    this.message = message;
  }
}
