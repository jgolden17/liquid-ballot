import { Field, InputType } from '@nestjs/graphql';

@InputType()
export class AddContestInput {
  @Field()
  electionId: string;

  @Field()
  contestId: string;
}
