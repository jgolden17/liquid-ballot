import { Field, InputType } from '@nestjs/graphql';
import { IContest, ICreateElectionDto, NewElection } from 'src/common';

@InputType()
export class ElectionInput implements NewElection {
  @Field()
  title: string;

  @Field()
  notes: string;

  @Field()
  description: string;

  @Field()
  startDate: Date;

  @Field()
  endDate: Date;

  contests: IContest[];
}

@InputType()
export class CreateElectionInput implements ICreateElectionDto {
  @Field(() => ElectionInput)
  election: NewElection;
}
