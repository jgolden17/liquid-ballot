import { Field, ObjectType } from '@nestjs/graphql';
import { ContestStatus, ContestType, IContest } from 'src/common';

@ObjectType()
export class Contest implements IContest {
  @Field()
  id: string;

  @Field()
  type: ContestType;

  @Field()
  title: string;

  @Field()
  body: string;

  @Field(() => [String])
  choices: string[];

  @Field()
  policy: string;

  @Field()
  status: ContestStatus;

  @Field()
  slug: string;
}
