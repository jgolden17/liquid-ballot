import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { ViewModule } from 'src/server/view/view.module';
import { ElectionsModule } from './elections/elections.module';
import { ContestsModule } from './contests/contests.module';
import { GraphQLGatewayModule } from './graphql-gateway/graphql-gateway.module';

@Module({
  imports: [
    MongooseModule.forRoot('mongodb://localhost:27017'),
    ContestsModule,
    ElectionsModule,
    GraphQLGatewayModule,
    ViewModule,
  ],
})
export class ServerModule {}
