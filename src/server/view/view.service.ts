import { Injectable, OnModuleInit } from '@nestjs/common';
import { Request, Response } from 'express';
import createServer from 'next';
import { NextServer } from 'next/dist/server/next';

@Injectable()
export class ViewService implements OnModuleInit {
  private server!: NextServer;

  private getUrlSearchParams(url: URL): NodeJS.Dict<string> {
    const result = {} as NodeJS.Dict<string>;

    for (const [key, value] of url.searchParams.entries()) {
      result[key] = value;
    }

    return result;
  }

  async onModuleInit(): Promise<void> {
    try {
      this.server = createServer({
        dev: true,
        dir: './src/client',
      });

      await this.server.prepare();
    } catch (error) {}
  }

  getServer(): NextServer {
    return this.server;
  }

  async render(req: Request, res: Response, isPredefined = false) {
    const url = new URL(`http://localhost:3000${req.url}`);

    await this.server.render(
      req,
      res,
      url.pathname,
      isPredefined ? {} : this.getUrlSearchParams(url),
    );
  }
}
