import { Controller, Get, Req, Res } from '@nestjs/common';
import { Request, Response } from 'express';
import { ViewService } from './view.service';

@Controller('/')
export class ViewController {
  constructor(private readonly viewService: ViewService) {}

  @Get('election/create')
  public async createElection(@Req() req: Request, @Res() res: Response) {
    await this.viewService.render(req, res, true);
  }

  @Get('elections')
  public async showElections(@Req() req: Request, @Res() res: Response) {
    await this.viewService.render(req, res, true);
  }

  @Get('elections/:slug')
  public async showElection(@Req() req: Request, @Res() res: Response) {
    await this.viewService.render(req, res, true);
  }

  @Get('contests/create')
  public async createContest(@Req() req: Request, @Res() res: Response) {
    await this.viewService.render(req, res, true);
  }

  @Get('contests/:slug')
  public async showContest(@Req() req: Request, @Res() res: Response) {
    await this.viewService.render(req, res, true);
  }

  @Get('_next*')
  public async assets(@Req() req: Request, @Res() res: Response) {
    await this.viewService.render(req, res);
  }
}
