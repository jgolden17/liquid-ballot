import { Logger } from '@nestjs/common';
import {
  CommandHandler,
  EventPublisher,
  ICommand,
  ICommandHandler,
} from '@nestjs/cqrs';
import { Election } from '../../domain/election.aggregate';

export class AddContest implements ICommand {
  constructor(public readonly id: string, public readonly contestId: string) {}
}

@CommandHandler(AddContest)
export class AddContestHandler implements ICommandHandler<AddContest> {
  private readonly logger = new Logger(AddContestHandler.name);

  constructor(private readonly publisher: EventPublisher) {}

  async execute(command: AddContest): Promise<any> {
    this.logger.log('execute', { command });

    try {
      const { id, contestId } = command;

      const ballot = new Election(id);

      ballot.addContest(contestId);

      this.publisher.mergeObjectContext(ballot).commit();
    } catch (error) {
      this.logger.error(error);

      throw error;
    }
  }
}
