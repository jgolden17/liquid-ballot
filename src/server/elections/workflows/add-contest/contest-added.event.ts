import { Logger } from '@nestjs/common';
import { EventsHandler, IEvent, IEventHandler } from '@nestjs/cqrs';

export class ContestAdded implements IEvent {
  constructor(
    public readonly contestId: string,
    public readonly addedAt: Date,
  ) {}
}

@EventsHandler(ContestAdded)
export class ContestAddedHandler implements IEventHandler<ContestAdded> {
  private readonly logger = new Logger(ContestAddedHandler.name);

  async handle(event: ContestAdded): Promise<void> {
    this.logger.log('handle', { event });
  }
}
