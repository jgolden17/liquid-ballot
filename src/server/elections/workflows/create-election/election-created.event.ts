import { EventsHandler, IEvent, IEventHandler } from '@nestjs/cqrs';
import { IElection } from 'src/common';
import { IElectionsRepository } from '../../elections.repository';

export class ElectionCreated implements IEvent {
  constructor(
    public readonly election: IElection,
    public readonly createdAt: Date,
  ) {}
}

@EventsHandler(ElectionCreated)
export class ElectionCreatedHandler implements IEventHandler<ElectionCreated> {
  constructor(private readonly repository: IElectionsRepository) {}

  handle(event: ElectionCreated) {
    const { election, createdAt } = event;

    return this.repository.create({ ...event.election, createdAt });
  }
}
