import { Logger } from '@nestjs/common';
import {
  CommandHandler,
  EventPublisher,
  ICommand,
  ICommandHandler,
} from '@nestjs/cqrs';
import { ICreateElectionDto } from 'src/common';
import { Election } from '../../domain/election.aggregate';

export class CreateElection implements ICommand {
  constructor(public readonly createElectionDto: ICreateElectionDto) {}
}

@CommandHandler(CreateElection)
export class CreateElectionHandler implements ICommandHandler<CreateElection> {
  private readonly logger = new Logger(CreateElectionHandler.name);

  constructor(private readonly publisher: EventPublisher) {}

  async execute(command: CreateElection): Promise<any> {
    const { createElectionDto } = command;

    try {
      const election = new Election('ELEC-0001');

      election.create(createElectionDto.election);

      this.publisher.mergeObjectContext(election).commit();
    } catch (error) {
      this.logger.error(error);
    }
  }
}
