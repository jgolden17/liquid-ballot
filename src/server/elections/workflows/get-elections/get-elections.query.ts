import { Logger } from '@nestjs/common';
import { IQuery, IQueryHandler, QueryHandler } from '@nestjs/cqrs';
import { IElection } from 'src/common';
import { ICollection } from 'src/server/common/collection.interface';
import { IElectionsRepository } from '../../elections.repository';

export class GetElections implements IQuery {}

@QueryHandler(GetElections)
export class GetElectionsHandler implements IQueryHandler<GetElections> {
  private readonly logger = new Logger(GetElectionsHandler.name);

  constructor(private readonly repository: IElectionsRepository) {}

  execute(query: GetElections): Promise<ICollection<IElection>> {
    return this.repository.find();
  }
}
