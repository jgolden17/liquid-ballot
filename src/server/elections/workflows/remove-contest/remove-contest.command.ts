import { Logger } from '@nestjs/common';
import {
  CommandHandler,
  EventPublisher,
  ICommand,
  ICommandHandler,
} from '@nestjs/cqrs';
import { Election } from '../../domain/election.aggregate';

export class RemoveContest implements ICommand {
  constructor(public readonly id: string, public readonly contestId: string) {}
}

@CommandHandler(RemoveContest)
export class RemoveContestHandler implements ICommandHandler<RemoveContest> {
  private readonly logger = new Logger(RemoveContestHandler.name);

  constructor(private readonly publisher: EventPublisher) {}

  async execute(command: RemoveContest): Promise<any> {
    this.logger.log('execute', { command });

    try {
      const { id, contestId } = command;

      const ballot = new Election(id);

      ballot.removeContest(contestId);

      this.publisher.mergeObjectContext(ballot).commit();
    } catch (error) {
      this.logger.error(error);

      throw error;
    }
  }
}
