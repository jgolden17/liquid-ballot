import { Logger } from '@nestjs/common';
import { EventsHandler, IEvent, IEventHandler } from '@nestjs/cqrs';

export class ContestRemoved implements IEvent {
  constructor(
    public readonly contestId: string,
    public readonly removedAt: Date,
  ) {}
}

@EventsHandler(ContestRemoved)
export class ContestRemovedHandler implements IEventHandler<ContestRemoved> {
  private readonly logger = new Logger(ContestRemovedHandler.name);

  handle(event: ContestRemoved) {
    this.logger.log('handle', { event });
  }
}
