import { Injectable } from '@nestjs/common';
import { CommandBus, QueryBus } from '@nestjs/cqrs';
import { ICreateElectionDto, IElection } from 'src/common';
import { ICollection } from '../common/collection.interface';
import { AddContest } from './workflows/add-contest';
import { CreateElection } from './workflows/create-election';
import { GetElections } from './workflows/get-elections';
import { RemoveContest } from './workflows/remove-contest';

@Injectable()
export abstract class IElectionsService {
  abstract addContest(ballotId: string, contestId: string): Promise<any>;
  abstract removeContest(ballotId: string, contestId: string): Promise<any>;
}

@Injectable()
export class ElectionsService implements IElectionsService {
  constructor(
    private readonly commandBus: CommandBus,
    private readonly queryBus: QueryBus,
  ) {}

  getElections(): Promise<ICollection<IElection>> {
    return this.queryBus.execute(new GetElections());
  }

  create(createElectionDto: ICreateElectionDto) {
    return this.commandBus.execute(new CreateElection(createElectionDto));
  }

  addContest(ballotId: string, contestId: string) {
    return this.commandBus.execute(new AddContest(ballotId, contestId));
  }

  removeContest(ballotId: string, contestId: string) {
    return this.commandBus.execute(new RemoveContest(ballotId, contestId));
  }
}
