import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, Model } from 'mongoose';
import { IContest } from 'src/common';
import {
  ElectionStatus,
  IElection,
} from 'src/common/interfaces/election.interface';

@Schema()
export class Election implements IElection {
  id: string;

  @Prop({ index: true, unique: true, required: true })
  sequence: number;

  @Prop({ index: true, unique: true, required: true })
  slug: string;

  @Prop()
  notes: string;

  @Prop()
  description: string;

  @Prop()
  startDate: Date;

  @Prop()
  endDate: Date;

  @Prop()
  status: ElectionStatus;

  @Prop()
  title: string;

  @Prop({ default: [] })
  contests: IContest[];

  @Prop()
  createdAt: Date;
}

export type ElectionDocument = Election & Document;

export type ElectionModel = Model<ElectionDocument>;

export const ElectionSchema = SchemaFactory.createForClass(Election);
