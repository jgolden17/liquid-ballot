import { Injectable, Logger } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { IElection } from 'src/common';
import { Collection } from '../common/collection';
import { ICollection } from '../common/collection.interface';
import { Election, ElectionModel } from './schema/election.schema';
import { createId } from './utils';

@Injectable()
export abstract class IElectionsRepository {
  abstract create(election: IElection): Promise<any>;
  abstract addContest(id: string, contestId: string): Promise<any>;
  abstract removeContest(id: string, contestId: string): Promise<any>;
  abstract find(): Promise<ICollection<IElection>>;
}

@Injectable()
export class ElectionRepository implements IElectionsRepository {
  private readonly logger = new Logger(ElectionRepository.name);

  constructor(
    @InjectModel(Election.name) private readonly model: ElectionModel,
  ) {}

  private async getNextSequence(): Promise<number> {
    const [last] = await this.model.find().sort({ sequence: -1 }).limit(1);

    return last.sequence + 1;
  }

  async create(election: IElection) {
    const sequence = await this.getNextSequence();

    await this.model.create({
      ...election,
      sequence,
      slug: createId('ELEC', sequence),
    });
  }

  async addContest(id: string, contestId: string): Promise<any> {
    await this.model.updateOne(
      { id },
      {
        $addToSet: {
          contests: contestId,
        },
      },
    );
  }

  async removeContest(id: string, contestId: string): Promise<any> {
    await this.model.updateOne(
      { id },
      {
        $pull: {
          contests: contestId,
        },
      },
    );
  }

  async find(): Promise<ICollection<IElection>> {
    const [elections, totalElections] = await Promise.all([
      this.model.find({}),
      this.model.countDocuments(),
    ]);

    this.logger.debug('find', { elections, totalElections });

    return new Collection(elections, totalElections);
  }
}
