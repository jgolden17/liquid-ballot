import { Module } from '@nestjs/common';
import { CqrsModule } from '@nestjs/cqrs';
import { MongooseModule } from '@nestjs/mongoose';
import {
  ElectionRepository,
  IElectionsRepository,
} from './elections.repository';
import { ElectionsService } from './elections.service';
import { ElectionResolver } from '../graphql-gateway/resolvers/elections.resolver';
import { Election, ElectionSchema } from './schema/election.schema';
import {
  AddContestHandler,
  ContestAddedHandler,
} from './workflows/add-contest';
import {
  ContestRemovedHandler,
  RemoveContestHandler,
} from './workflows/remove-contest';
import {
  CreateElectionHandler,
  ElectionCreatedHandler,
} from './workflows/create-election';
import { GetElectionsHandler } from './workflows/get-elections';

@Module({
  imports: [
    CqrsModule,
    MongooseModule.forFeature([
      {
        name: Election.name,
        schema: ElectionSchema,
      },
    ]),
  ],
  providers: [
    CreateElectionHandler,
    ElectionCreatedHandler,
    ContestAddedHandler,
    AddContestHandler,
    RemoveContestHandler,
    ContestRemovedHandler,
    GetElectionsHandler,
    {
      provide: IElectionsRepository,
      useClass: ElectionRepository,
    },
    ElectionsService,
    ElectionResolver,
  ],
  exports: [ElectionsService, ElectionResolver],
})
export class ElectionsModule {}
