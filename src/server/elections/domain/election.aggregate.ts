import { AggregateRoot } from '@nestjs/cqrs';
import { IContest } from 'src/common';
import { InvariantViolation } from 'src/common/exceptions/invariant-violation.exception';
import {
  ElectionStatus,
  IElection,
} from 'src/common/interfaces/election.interface';
import { ContestAdded } from '../workflows/add-contest';
import { ElectionCreated } from '../workflows/create-election';
import { ContestRemoved } from '../workflows/remove-contest';

export class Election extends AggregateRoot implements IElection {
  id: string;
  electionId: string;
  slug: string;
  status: ElectionStatus;
  title: string;
  contests: IContest[];
  notes: string;
  description: string;
  startDate: Date;
  endDate: Date;
  createdAt: Date;

  constructor(id: string) {
    super();

    this.id = id;
    this.contests = [];
  }

  private hasContest(contestId) {
    return this.contests.some((contest) => contest === contestId);
  }

  create(election: Partial<IElection>) {
    this.apply(
      new ElectionCreated(
        { ...election, status: ElectionStatus.Draft } as IElection,
        new Date(),
      ),
    );
  }

  addContest(contestId: string) {
    if (!contestId) {
      throw new InvariantViolation('Contest ID is required');
    }

    if (this.hasContest(contestId)) {
      throw new InvariantViolation(
        `Contest ${contestId} is already on the election`,
      );
    }

    this.apply(new ContestAdded(contestId, new Date()));
  }

  removeContest(contestId: string) {
    if (!contestId) {
      throw new InvariantViolation('Contest ID is required');
    }

    if (!this.hasContest(contestId)) {
      throw new InvariantViolation(
        `Contest ${contestId} is not on the election`,
      );
    }

    this.apply(new ContestRemoved(contestId, new Date()));
  }
}
