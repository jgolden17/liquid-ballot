import { createId } from '../create-id.utils';

describe('create ID', () => {
  it('should create the ID', () => {
    expect(createId('ELEC', 1)).toBe('ELEC-0001');
    expect(createId('ELEC', 12)).toBe('ELEC-0012');
    expect(createId('ELEC', 123)).toBe('ELEC-0123');
    expect(createId('ELEC', 1234)).toBe('ELEC-1234');
    expect(createId('ELEC', 12345)).toBe('ELEC-12345');
  });
});
