import { padLeft } from './left-pad.util';

export const createId = (prefix: string, serial: number) => {
  return `${prefix}-${padLeft(serial)}`;
};
