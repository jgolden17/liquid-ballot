export const padLeft = (value: number, length = 4): string => {
  const asString = Number(value).toString();

  if (asString.length >= length) {
    return asString;
  }

  const padding = new Array(length - asString.length).fill('0').join('');

  return `${padding}${asString}`;
};
