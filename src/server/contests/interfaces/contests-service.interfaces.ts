import { IContest, ICreateContestDto } from 'src/common';

export interface IContestsService {
  createContest(createContestDto: ICreateContestDto): Promise<any> | any;
}

export interface IContestsRepository {
  create(contest: Omit<IContest, 'id'>): Promise<any> | any;
}
