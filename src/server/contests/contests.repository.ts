import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { IContest } from 'src/common';
import { createId } from '../elections/utils';
import { IContestsRepository } from './interfaces/contests-service.interfaces';
import { Contest, ContestModel } from './schemas/contest.schema';

@Injectable()
export abstract class AbstractContestsRepository
  implements IContestsRepository
{
  abstract create(contest: Omit<IContest, 'id'>): Promise<any> | any;

  abstract find(): Promise<any>;

  abstract findBySlug(slug: string): Promise<any>;
}

@Injectable()
export class ContestsRepository implements AbstractContestsRepository {
  constructor(
    @InjectModel(Contest.name) private readonly model: ContestModel,
  ) {}

  private async getNextSequence(): Promise<number> {
    const [lastContest] = await this.model
      .find()
      .sort({ sequence: -1 })
      .limit(1);

    return lastContest.sequence + 1;
  }

  async create(contest: Omit<IContest, 'id'>) {
    const sequence = await this.getNextSequence();

    await this.model.create({
      ...contest,
      sequence,
      slug: createId('CONT', sequence),
    });
  }

  async find() {
    return this.model.find({});
  }

  async findBySlug(slug: string): Promise<any> {
    return this.model.findOne({ slug });
  }
}
