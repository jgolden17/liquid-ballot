import { Module } from '@nestjs/common';
import { CqrsModule } from '@nestjs/cqrs';
import { MongooseModule } from '@nestjs/mongoose';
import { ContestsController } from './contests.controller';
import {
  AbstractContestsRepository,
  ContestsRepository,
} from './contests.repository';
import { AbstractContestsService, ContestsService } from './contests.service';
import { ContestResolver } from '../graphql-gateway/resolvers/contests.resolver';
import { Contest, ContestSchema } from './schemas/contest.schema';
import {
  ContestCreatedHandler,
  CreateContestHandler,
} from './workflows/create-contest';
import {
  GetContestHandler,
  GetContestsHandler,
} from './workflows/view-contests';

@Module({
  imports: [
    CqrsModule,
    MongooseModule.forFeature([
      {
        name: Contest.name,
        schema: ContestSchema,
      },
    ]),
  ],
  controllers: [ContestsController],
  providers: [
    ContestCreatedHandler,
    CreateContestHandler,
    GetContestsHandler,
    GetContestHandler,
    {
      provide: AbstractContestsService,
      useClass: ContestsService,
    },
    {
      provide: AbstractContestsRepository,
      useClass: ContestsRepository,
    },
    ContestResolver,
  ],
})
export class ContestsModule {}
