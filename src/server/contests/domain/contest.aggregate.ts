import { AggregateRoot } from '@nestjs/cqrs';
import { ContestStatus, ContestType, IContest } from 'src/common';
import Joi = require('joi');
import { InvariantViolation } from 'src/common/exceptions/invariant-violation.exception';
import { ContestCreated } from '../workflows/create-contest';

export class Contest extends AggregateRoot implements IContest {
  id: string;
  type: ContestType;
  title: string;
  body: string;
  choices: string[];
  policy: string;
  status: ContestStatus;
  slug: string;

  private readonly schema = Joi.object({
    type: Joi.string()
      .valid(...Object.values(ContestType))
      .required(),
    title: Joi.string().required(),
    body: Joi.string().required(),
    choices: Joi.array().min(2).required(),
    policy: Joi.string(),
    status: Joi.string(),
    slug: Joi.string(),
  });

  private validate(contest: Partial<IContest>) {
    return this.schema.validate(contest);
  }

  create(contest: Partial<IContest>) {
    const { error } = this.validate(contest);

    if (error) {
      throw new InvariantViolation(error.message);
    }

    this.apply(new ContestCreated(contest as IContest, new Date()));
  }
}
