import { Logger } from '@nestjs/common';
import { IQuery, IQueryHandler, QueryHandler } from '@nestjs/cqrs';
import { AbstractContestsRepository } from '../../contests.repository';

export class GetContest implements IQuery {
  constructor(public readonly slug: string) {}
}

@QueryHandler(GetContest)
export class GetContestHandler implements IQueryHandler<GetContest> {
  private readonly logger = new Logger(GetContestHandler.name);

  constructor(private readonly repository: AbstractContestsRepository) {}

  execute(query: GetContest): Promise<any> {
    this.logger.log('execute', { query });

    const { slug } = query;

    return this.repository.findBySlug(slug);
  }
}
