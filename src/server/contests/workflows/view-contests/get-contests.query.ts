import { Logger } from '@nestjs/common';
import { IQuery, IQueryHandler, QueryHandler } from '@nestjs/cqrs';
import { AbstractContestsRepository } from '../../contests.repository';

export class GetContests implements IQuery {}

@QueryHandler(GetContests)
export class GetContestsHandler implements IQueryHandler<GetContests> {
  private readonly logger = new Logger(GetContestsHandler.name);

  constructor(private readonly repository: AbstractContestsRepository) {}

  execute(query: GetContests): Promise<any> {
    this.logger.log('execute', { query });

    return this.repository.find();
  }
}
