import { Logger } from '@nestjs/common';
import { EventsHandler, IEvent, IEventHandler } from '@nestjs/cqrs';
import { IContest } from 'src/common';
import { AbstractContestsRepository } from '../../contests.repository';

export class ContestCreated implements IEvent {
  constructor(
    public readonly contest: IContest,
    public readonly createdAt: Date,
  ) {}
}

@EventsHandler(ContestCreated)
export class ContestCreatedHandler implements IEventHandler<ContestCreated> {
  private readonly logger = new Logger(ContestCreatedHandler.name);

  constructor(private readonly repository: AbstractContestsRepository) {}

  async handle(event: ContestCreated) {
    this.logger.log('handle', { event });

    const { contest } = event;

    await this.repository.create(contest);
  }
}
