import { ContestStatus, ICreateContestDto } from 'src/common';
import {
  CommandHandler,
  EventPublisher,
  ICommand,
  ICommandHandler,
} from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import slugify from 'slugify';
import { Contest } from '../../domain/contest.aggregate';

export class CreateContest implements ICommand {
  constructor(public readonly createContestDto: ICreateContestDto) {}
}

@CommandHandler(CreateContest)
export class CreateContestHandler implements ICommandHandler<CreateContest> {
  private readonly logger = new Logger(CreateContestHandler.name);

  constructor(private readonly publisher: EventPublisher) {}

  async execute(command: CreateContest): Promise<any> {
    this.logger.log('execute', { command });

    try {
      const { createContestDto } = command;

      const contest = new Contest();

      contest.create({
        ...createContestDto.contest,
        status: ContestStatus.Draft,
        slug: slugify(createContestDto.contest.title, {
          lower: true,
        }),
      });

      this.publisher.mergeObjectContext(contest).commit();
    } catch (error) {
      this.logger.error(error);

      throw error;
    }
  }
}
