import { Body, Controller, Get, Logger, Param, Post } from '@nestjs/common';
import { IContest } from 'src/common';
import { AbstractContestsService } from './contests.service';

interface ICreateContestDto {
  contest: IContest;
}

@Controller('/api/contests/')
export class ContestsController {
  private readonly logger = new Logger(ContestsController.name);

  constructor(private readonly service: AbstractContestsService) {}

  @Post()
  createContest(@Body() createContestDto: ICreateContestDto) {
    this.logger.verbose('createContest', { dto: createContestDto });
    return this.service.createContest(createContestDto);
  }

  @Get()
  getContests() {
    this.logger.verbose('getContests');
    return this.service.getContests();
  }

  @Get(':slug')
  getContest(@Param('slug') slug: string) {
    this.logger.verbose('getContest', { slug });
    return this.service.getContest(slug);
  }
}
