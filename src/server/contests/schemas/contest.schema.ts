import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, Model } from 'mongoose';
import { ContestStatus, ContestType, IContest } from 'src/common';

@Schema()
export class Contest implements IContest {
  id: string;

  @Prop({ index: true, unique: true, required: true })
  sequence: number;

  @Prop({ index: true, unique: true, required: true })
  slug: string;

  @Prop({ required: true })
  type: ContestType;

  @Prop({ required: true })
  title: string;

  @Prop({ required: true })
  body: string;

  @Prop({ required: true })
  choices: string[];

  @Prop()
  policy: string;

  @Prop({ required: true })
  status: ContestStatus;
}

export type ContestDocument = Contest & Document;

export type ContestModel = Model<ContestDocument>;

export const ContestSchema = SchemaFactory.createForClass(Contest);
