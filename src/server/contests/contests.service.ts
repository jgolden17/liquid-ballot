import { Injectable } from '@nestjs/common';
import { CommandBus, QueryBus } from '@nestjs/cqrs';
import { ICreateContestDto } from 'src/common';
import { IContestsService } from './interfaces/contests-service.interfaces';
import { CreateContest } from './workflows/create-contest';
import { GetContest } from './workflows/view-contests/get-contest.query';
import { GetContests } from './workflows/view-contests/get-contests.query';

@Injectable()
export abstract class AbstractContestsService implements IContestsService {
  constructor(
    protected readonly commandBus: CommandBus,
    protected readonly queryBus: QueryBus,
  ) {}

  abstract createContest(createContestDto: ICreateContestDto): Promise<any>;

  abstract getContests(): Promise<any>;

  abstract getContest(slug: string): Promise<any>;
}

@Injectable()
export class ContestsService extends AbstractContestsService {
  createContest(createContestDto: ICreateContestDto) {
    return this.commandBus.execute(new CreateContest(createContestDto));
  }

  getContests(): Promise<any> {
    return this.queryBus.execute(new GetContests());
  }

  getContest(slug: string): Promise<any> {
    return this.queryBus.execute(new GetContest(slug));
  }
}
