import { ICollection } from './collection.interface';

export class Collection<T> implements ICollection<T> {
  constructor(
    public readonly results: T[] = [],
    public readonly totalCount: number = 0,
  ) {}
}
