export interface ICollection<T> {
  results: T[];
  totalCount: number;
}
