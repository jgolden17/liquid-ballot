type ID = string;
type OmitId<T> = Omit<T, 'id'>;
