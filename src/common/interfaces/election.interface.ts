import { IContest } from './contest.interface';

export enum ElectionStatus {
  Draft = 'Draft',
  Scheduled = 'Scheduled',
  Open = 'Open',
  Closed = 'Closed',
}

export interface IElection {
  id: ID;
  slug: string;
  status: ElectionStatus;
  title: string;
  notes: string;
  description: string;
  startDate: Date;
  endDate: Date;
  contests: IContest[];
  createdAt: Date;
}

export type NewElection = Omit<
  IElection,
  'id' | 'electionId' | 'status' | 'slug' | 'contests' | 'createdAt'
>;

export interface ICreateElectionDto {
  election: NewElection;
}
