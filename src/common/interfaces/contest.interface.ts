export enum ContestType {
  Plurality = 'Plurality',
  RankedSimple = 'RankedSimple',
  RankedEnhanced = 'RankedEnhanced',
  Condorcet = 'Condorcet',
  Approval = 'Approval',
}

export enum ContestStatus {
  Draft = 'Draft',
}

type ID = string;

export interface IContest {
  type: ContestType;
  title: string;
  body: string;
  choices: string[];
  policy: string;
  status: ContestStatus;
  slug: string;
}

export type NewContest = Omit<IContest, 'id' | 'status' | 'slug'>;

export interface ICreateContestDto {
  contest: NewContest;
}
