export class InvariantViolation extends Error {
  constructor(message: string) {
    super(message);
    this.name = InvariantViolation.name;
  }
}
