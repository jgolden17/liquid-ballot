import { useState } from 'react';

const ContestActions = ({ canRemove = false, canIncludeOnBallot = false }) => {
  const [isActive, setActive] = useState(false);

  const actions = [];

  if (canRemove) {
    actions.push(
      <button className="button dropdown-item" style={{ border: 'none' }}>
        Remove from ballot
      </button>
    );
  }

  if (canIncludeOnBallot) {
    actions.push(
      <button className="button dropdown-item" style={{ border: 'none' }}>
        Include on ballot
      </button>
    );
  }

  if (actions.length === 0) {
    return null;
  }

  return (
    <div className={`dropdown ${isActive ? 'is-active' : ''}`.trim()}>
      <div className="dropdown-trigger">
        <button className="button" aria-haspopup="true" aria-controls="dropdown-menu3" onClick={() => setActive(!isActive)}>
          <span>Actions</span>
          <span className="icon is-small">
            <i className="fas fa-angle-down" aria-hidden="true"></i>
          </span>
        </button>
      </div>
      <div className="dropdown-menu" id="dropdown-menu3" role="menu">
        <div className="dropdown-content">
          {actions}
        </div>
      </div>
    </div>
  );
};

export default ContestActions;
