export type TextAreaProps = React.DetailedHTMLProps<React.TextareaHTMLAttributes<HTMLTextAreaElement>, HTMLTextAreaElement> & {
  id: string;
  label: string;
};

const TextArea = ({ id, label, ...rest }: TextAreaProps) => (
  <article className="field mb-5">
    <label htmlFor={id} className="label">
      {label}
    </label>
    <div className="control">
      <textarea
        id={id}
        className="textarea"
        {...rest}
      />
    </div>
  </article>
);


export default TextArea;
