import Link from "next/link";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";

function format(string: string) {
  return string
    .replaceAll('-', ' ')
    .toLowerCase()
    .split(' ')
    .map((word) => word.replace(word[0], word[0].toUpperCase()))
    .join(' ');
}

const Breadcrumbs = () => {
  const router = useRouter();
  const [breadcrumbs, setBreadcrumbs] = useState(null);

  useEffect(() => {
    if (router) {
      const paths = router.asPath.split('/');

      paths.shift();

      setBreadcrumbs(paths.map((path, index) => ({
        breadcrumb: path,
        href: `/${paths.slice(0, index + 1).join('/')}`,
      })));
    }
  }, [router]);

  if (!breadcrumbs) {
    return null;
  }

  return (
    <nav className="breadcrumb p-responsive" aria-label="breadcrumbs">
      <ol>
        <li>
          <Link href="/">
            <a href="/">Home</a>
          </Link>
        </li>
        {breadcrumbs.map((breadcrumb) => (
          <li>
            <Link href={breadcrumb.href}>
              <a href={breadcrumb.href}>
                {format(breadcrumb.breadcrumb)}
              </a>
            </Link>
          </li>
        ))}
      </ol>
    </nav>
  );
};

export default Breadcrumbs;
