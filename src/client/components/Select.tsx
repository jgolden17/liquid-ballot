export type SelectProps = React.DetailedHTMLProps<React.SelectHTMLAttributes<HTMLSelectElement>, HTMLSelectElement> & {
  id: string;
  label: string;
  options: { value: string, label: string }[];
};

const Select = ({ id, label, options, ...rest }: SelectProps) => (
  <article className="field mb-5">
    <label htmlFor={id} className="label">
      Type
    </label>
    <div className="select">
      <select
        id={id}
        name="type"
        {...rest}
      >
        {options.map((option) => (
          <option key={option.label} value={option.value}>{option.label}</option>
        ))}
      </select>
    </div>
  </article>
);

export default Select;
