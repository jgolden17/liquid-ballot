export type InputProps = React.DetailedHTMLProps<React.InputHTMLAttributes<HTMLInputElement>, HTMLInputElement> & {
  id: string;
  label: string;
};

const Input = ({ id, label, className, type = 'text', ...rest }: InputProps) => (
  <article className={`field mb-5 ${className}`.trim()}>
    <label htmlFor={id} className="label">
      {label}
    </label>
    <div className="control">
      <input
        id={id}
        className="input"
        type={type}
        {...rest}
      />
    </div>
  </article>
);


export default Input;
