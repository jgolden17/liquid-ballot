import { useState } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEllipsisH } from '@fortawesome/free-solid-svg-icons';

const ElectionActions = ({ canDelete }) => {
  const [isActive, setActive] = useState(false);

  const actions = [];

  if (canDelete) {
    actions.push(
      <button className="button dropdown-item" style={{ border: 'none' }}>
        Delete ballot
      </button>
    );
  }

  if (actions.length === 0) {
    return null;
  }

  return (
    <div className={`dropdown ${isActive ? 'is-active' : ''}`.trim()}>
      <div className="dropdown-trigger">
        <button
          className="button p-0"
          aria-haspopup="true"
          aria-controls="dropdown-menu3"
          onClick={() => setActive(!isActive)}
          style={{ border: 'none' }}
        >
          <span className="sr-only">Actions</span>
          <span className="icon mr-2">
            <FontAwesomeIcon icon={faEllipsisH} />
          </span>
        </button>
      </div>
      <div className="dropdown-menu" id="dropdown-menu3" role="menu">
        <div className="dropdown-content">
          {actions}
        </div>
      </div>
    </div>
  );
};

export default ElectionActions;
