import { InputProps } from './Input';

export type DataListProps = React.DetailedHTMLProps<React.InputHTMLAttributes<HTMLInputElement>, HTMLInputElement> & {
  id: string;
  label: string;
  options: string[];
};

const DataList = ({ id, label, options, ...rest }: DataListProps) => (
  <article className="field mb-5">
    <label htmlFor={id} className="label">
      {label}
    </label>
    <input
      className="input"
      type="text"
      list={id}
      {...rest as unknown as InputProps}
    />
    <datalist id={id}>
      {options.map((option) => (
        <option key={option} value={option} />
      ))}
    </datalist>
  </article>
);

export default DataList;
