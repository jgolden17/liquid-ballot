import { PropsWithChildren } from 'react';
import Breadcrumbs from './Breadcrumbs';
import Navbar from './Navbar';

type LayoutProps = PropsWithChildren<any>;

const Layout = ({ children }: LayoutProps) => (
  <>
    <header className="mb-5">
      <Navbar />
    </header>
    <div className="container">
      <Breadcrumbs />
    </div>
    <main>{children}</main>
    <footer />
  </>
);

export default Layout;
