const Checkbox = ({ id, label, ...rest }) => (
  <div className="field">
    <div className="control">
      <label htmlFor={id} className="checkbox">
        <input id={id} type="checkbox" {...rest} />
        {label}
      </label>
    </div>
  </div>
);

export default Checkbox;
