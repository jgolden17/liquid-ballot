import Link from 'next/link';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCheckToSlot } from '@fortawesome/free-solid-svg-icons';
import { IElection } from '../../common';
import ElectionActions from './ElectionActions';

const tdStyle = {
  display: 'flex',
  alignItems: 'center',
}

const ElectionsList = ({ elections }) => (
  <table id="election-list" className="table is-fullwidth is-hoverable">
    <thead className="is-sr-only">
      <tr>
        <th>Key</th>
        <th>Title</th>
        <th>Actions</th>
      </tr>
    </thead>
    <tbody>
      {elections.map((election: IElection) => (
        <>
          <tr key={election.id} style={{ display: 'flex', flexWrap: 'wrap' }}>
            <td style={tdStyle}>
              <span className="icon has-text-info mr-2">
                <FontAwesomeIcon icon={faCheckToSlot} />
              </span>
              <Link href="/elections/[slug]" as={`/elections/${election.slug}`}>
                <a href={`/elections/${election.slug}`} title={election.slug}>
                  {election.slug}
                </a>
              </Link>
            </td>
            <td style={{ ...tdStyle, flexGrow: 1 }}>
              {election.title}
            </td>
            <td style={tdStyle} align="right">
              <ElectionActions canDelete={true} />
            </td>
          </tr>
        </>
      ))}
    </tbody>
  </table>
);

export default ElectionsList;
