import Link from 'next/link';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCheckToSlot } from '@fortawesome/free-solid-svg-icons';
import ContestActions from './ContestActions';
import { IContest } from '../../common';

type ContestListProps = {
  contests: IContest[];
  canRemove?: boolean;
}


const ContestsList = ({ contests, canRemove = false }: ContestListProps) => (
  <table className="table is-fullwidth is-hoverable">
    <thead className="is-sr-only">
      <tr>
        <th>Key</th>
        <th>Title</th>
        <th>Type</th>
        <th>Policy</th>
        <th>Actions</th>
      </tr>
    </thead>
    <tbody>
      {contests.map((contest: IContest) => (
        <tr key={contest.slug}>
          <td style={{ verticalAlign: 'middle' }}>
            <span className="icon has-text-info mr-2">
              <FontAwesomeIcon icon={faCheckToSlot} />
            </span>
            <Link href="/contests/[slug]" as={`/contests/${contest.slug}`}>
              <a href={`/contests/${contest.slug}`} title={contest.slug}>
                {contest.slug}
              </a>
            </Link>
          </td>
          <td style={{ verticalAlign: 'middle' }}>
            <Link
              href={{
                pathname: '/contests/[slug]',
                query: { slug: contest.slug },
              }}
            >
              <a href={`/contests/${contest.slug}`} title={contest.title}>
                {contest.title}
              </a>
            </Link>
          </td>
          <td style={{ verticalAlign: 'middle' }}>
            <span>{contest.type}</span>
          </td>
          <td style={{ verticalAlign: 'middle' }}>
            <span>{contest.policy}</span>
          </td>
          <td style={{ verticalAlign: 'middle' }} align="right">
            <ContestActions canRemove={canRemove} />
          </td>
        </tr>
      ))}
    </tbody>
  </table>
);

export default ContestsList;
