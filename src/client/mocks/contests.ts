import { type IContest } from '../../common';

enum ContestType {
  Plurality = 'Plurality',
  RankedSimple = 'RankedSimple',
  RankedEnhanced = 'RankedEnhanced',
  Condorcet = 'Condorcet',
  Approval = 'Approval',
}

export const contests = [
  {
    id: '1234',
    type: ContestType.Plurality,
    choices: ['Yes', 'No'],
    title: 'Adopt Anti-Capitalist License Agreement',
    location: '/contests/adopt-anti-capitalist-license-agreement',
    policy: 'Technology Infrastructure and Services',
    status: 'Draft',
    slug: 'CONT-0043',
  },
  {
    status: 'Draft',
    type: ContestType.Plurality,
    choices: ['Yes', 'No'],
    title:
      'Prioritizing consent in the design and support of our technology infrastructure and services',
    policy: 'Technology Infrastructure and Services',
    body: `
    In their “Building Consentful Tech zine” the Consentful Tech Project sponsored by Allied Media Projects, introduces a framework for integrating practices of consent into the design of technology. “Consentful technologies are digital applications and spaces that are built with consent at their core, and that support the self-determination of people who use and are affected by these technologies.”

Using the FRIES acronym originally promoted by a Planned Parenthood campaign to educate youth about the importance of consent in sexual encounters the authors of the “Building Consentful Tech zine” put forth the idea that interactions with technology should also be Freely given, Reversible, Informed, Enthusiastic and Specific. By understanding our data bodies as an extension of our physical bodies it becomes more intuitive to apply these criteria for consentful interactions with technology.

This provides us with a powerful and politically motivated framework for making design decisions about the technology we build and provide for our members. But taking this concept a step further we can also apply these same criteria to guide our interactions with May First members when providing technical support that might require access to private member data.

What do you think about Consentful Tech as a conceptual model for how May First should build and provide support for our member owned infrastructure and services?
    `,
    slug: 'CONT-0023',
  },
] as IContest[];
