import { FC } from 'react';
import Link from 'next/link';
import { DataList, Layout, Input, Select, TextArea } from '../../components';
import { useElection } from '../../hooks';

type CreateElectionProps = {
  policies: string[];
}

const CreateElection: FC = ({ policies }: CreateElectionProps) => {
  const { election, onChange, onSubmit } = useElection();

  return (
    <Layout>
      <div className="container p-responsive">
        <h1 className="title is-3">Create an election</h1>
        <div className="card">
          <div className="card-content">
            <form className="content" onSubmit={onSubmit}>
              <Input
                label="Election title"
                id="election-title"
                value={election.title}
                name="title"
                onChange={onChange}
              />
              <TextArea
                label="Description"
                id="election-description"
                name="description"
                rows={10}
                value={election.description}
                onChange={onChange}
              />
              <div className="is-flex">
                <Input
                  className="mr-3"
                  label="Start Date"
                  id="election-start-date"
                  type="date"
                  name="startDate"
                  onChange={onChange}
                />
                <Input
                  label="End Date"
                  id="election-end-date"
                  type="date"
                  name="endDate"
                  onChange={onChange}
                />
              </div>
              <div className="field is-grouped is-grouped-right">
                <div className="control">
                  <button type="submit" className="button is-link">Create election</button>
                </div>
                <div className="control">
                  <button className="button is-link is-light">Cancel</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </Layout>
  );
};

export async function getStaticProps() {
  return {
    props: {
      policies: [
        'Technology Infrastructure and Services',
        'Uncategorized',
      ],
    },
  };
}

export default CreateElection;
