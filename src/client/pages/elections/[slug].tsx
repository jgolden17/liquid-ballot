import { NextPageContext } from 'next';
import { gql } from '@apollo/client';
import Layout from '../../components/Layout';
import { IElection } from '../../../common';
import client from '../../apollo-client';
import { FC } from 'react';
import ContestsList from '../../components/ContestsList';

type Props = {
  hasError: boolean;
  election: IElection;
};

const Election: FC = ({ election }: Props) => {
  return (
    <Layout>
      <div className="container p-responsive">
        <div className="p-5">
          <header className="has-background-white p-responsive">
            <p className="title is-size-4 mb-5">
              {election.title}
            </p>
          </header>
          <section className="section pl-0 pt-5 pb-5">
            <p className="title is-size-6 is-sr-only">Details</p>
            <table className="table">
              <tr>
                <th>Status</th>
                <td>{election.status}</td>
              </tr>
            </table>
          </section>
          <section className="section pl-0 pt-5 pb-5">
            <p className="title is-size-6">Contests</p>
            <div className="content">
              {election.contests.length === 0 ? (
                <p>There are no contests yet on the election.</p>
              ) : (
                <ContestsList contests={election.contests} canRemove={true} />
              )}
            </div>
          </section>
        </div>
      </div>
    </Layout>
  );
};

export async function getServerSideProps(context: NextPageContext): Promise<{ props: Props }> {
  const { slug } = context.query;

  const { data, errors } = await client.query({
    query: gql`
      query Election {
        election(slug: "${slug}") {
          id
          title
          status
          contests {
            id
            title
            status
            slug
            type
            policy
          }
        }
      }
    `,
  });

  return {
    props: {
      hasError: !!errors,
      election: data?.election,
    },
  };
}

export default Election;
