import { FC } from 'react';
import Link from 'next/link'
import { gql } from '@apollo/client';
import Layout from '../../components/Layout';
import { type IElection } from '../../../common';
import ElectionsList from '../../components/ElectionsList';
import client from '../../apollo-client';

type ElectionsProps = {
  elections: IElection[];
  hasError: boolean;
  error?: string;
}

const Elections: FC = ({ elections, hasError, error }: ElectionsProps) => (
  <Layout>
    <div className="container p-responsive">
      <div className="flex flex-justify-between mb-5">
        <h1 className="title is-3 m-0">Elections</h1>
        <Link href="/elections/create/">
          <a href="/elections/create/" className="button is-info is-medium">
            Create an election
          </a>
        </Link>
      </div>
      {hasError ? (
        <div>
          <span>oops</span>
          <p>{error}</p>
        </div>
      ) : (
        <ElectionsList elections={elections} />
      )}
    </div>
  </Layout>
);

export async function getServerSideProps() {
  try {
    const { data, errors } = await client.query({
      errorPolicy: 'all',
      query: gql`
        query Elections {
          elections {
            nodes {
              title
              status
              slug
            }
          }
        }
      `,
    });

    console.log('data', data.elections, 'errors', errors);

    return {
      props: {
        hasError: false,
        elections: data?.elections?.nodes as IElection[],
      },
    };
  } catch (error_) {
    return {
      props: {
        hasError: true,
        elections: null,
        error: error_.message,
      },
    };
  }
}

export default Elections;
