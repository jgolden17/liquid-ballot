import NextApp, { AppProps } from 'next/app';
import Head from 'next/head';
import '../styles.css';
import { config } from '@fortawesome/fontawesome-svg-core'
import '@fortawesome/fontawesome-svg-core/styles.css'
config.autoAddCss = false

class App extends NextApp<AppProps> {
  render() {
    const { Component, pageProps } = this.props;

    return (
      <>
        <Head>
          <title>My page title</title>
          <meta name="viewport" content="initial-scale=1.0, width=device-width" />
          <script src="https://kit.fontawesome.com/cec50d27f0.js" crossOrigin="anonymous"></script>
        </Head>
        <Component {...pageProps} />);
      </>
    );
  }
}

export default App;
