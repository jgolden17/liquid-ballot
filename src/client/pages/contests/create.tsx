import { FC } from 'react';
import Link from 'next/link';
import { DataList, Layout, Input, Select, TextArea } from '../../components';
import { useContestForm } from '../../hooks';

enum ContestType {
  Plurality = 'Plurality',
  RankedSimple = 'RankedSimple',
  RankedEnhanced = 'RankedEnhanced',
  Condorcet = 'Condorcet',
  Approval = 'Approval',
}

type CreateContestProps = {
  policies: string[];
}

const CreateContest: FC = ({ policies }: CreateContestProps) => {
  const { form, onChange, onSubmit } = useContestForm();

  return (
    <Layout>
      <div className="container p-responsive">
        <form className="content">
          <Select
            label="Type"
            id="new-contest-type"
            value={form.type}
            name="type"
            onChange={onChange}
            options={
              Object.keys(ContestType)
                .filter((key) => Number.isNaN(+key))
                .map((type) => ({ value: type, label: type }))
            }
          />
          <Input
            label="Title"
            id="new-contest-title"
            value={form.title}
            name="title"
            onChange={onChange}
          />
          <DataList
            label="Policy"
            id="new-contest-policy"
            name="policy"
            options={policies}
            onChange={onChange}
          />
          <TextArea
            label="Body"
            id="new-contest-body"
            placeholder={`Where As...`}
            name="body"
            rows={20}
            value={form.body}
            onChange={onChange}
          />
          <TextArea
            label="Choices"
            id="new-contest-choices"
            placeholder={`Muddy\nKona`}
            value={form.choices.join('\n')}
            name="choices"
            onChange={onChange}
          />
          <footer className="card-footer">
            <div className="card-footer-item">
              <button
                type="submit"
                className="button is-fullwidth is-primary"
                onClick={onSubmit}
              >
                Create
              </button>
            </div>
            <div className="card-footer-item">
              <Link href="/contests/">
                <a href="/contests/" className="button is-fullwidth">
                  Discard
                </a>
              </Link>
            </div>
          </footer>
        </form>
      </div>
    </Layout>
  );
};

export async function getStaticProps() {
  return {
    props: {
      policies: [
        'Technology Infrastructure and Services',
        'Uncategorized',
      ],
    },
  };
}

export default CreateContest;
