import { NextPageContext } from 'next';
import Layout from '../../components/Layout';
import { IContest } from '../../../common';

type Props = {
  contest: IContest;
};

const Contest = ({ contest }: Props) => {
  return (
    <Layout>
      <div className="container p-responsive">
        <div className="p-5 has-background-white">
          <p className="title is-size-4 mb-5">
            {contest.title}
          </p>
          {/* <div className="buttons">
            <button className="button">Edit</button>
            <button className="button">Comment</button>
            <button className="button">Close</button>
          </div> */}
          <section className="section pl-0 pt-5 pb-5">
            <p className="title is-size-6 is-sr-only">Details</p>
            <table className="table">
              <tr>
                <th>Contest Type</th>
                <td>{contest.type}</td>
              </tr>
              <tr>
                <th>Status</th>
                <td>{contest.status}</td>
              </tr>
              <tr>
                <th>Policy</th>
                <td>{contest.policy}</td>
              </tr>
            </table>
          </section>
          <section className="section pl-0 pt-5 pb-5">
            <p className="title is-size-6">Body</p>
            <div className="content">
              {contest.body.split('\n').map((paragraph, index) => (
                <p key={`p-${index}`}className="content">{paragraph}</p>
              ))}
            </div>
          </section>
          {/* <section className="section pl-0 pt-5 pb-5">
            <p className="title is-size-6">Activity</p>
            <div className="tabs">
              <ul>
                <li>
                  <a>All</a>
                </li>
                <li className="is-active">
                  <a>Comments</a>
                </li>
                <li>
                  <a>History</a>
                </li>
              </ul>
            </div>
            <div className="content">
              <p>There are no comments yet on the contest.</p>
            </div>
          </section>
          <div className="content">
            <button className="button">Comment</button>
          </div> */}
        </div>
      </div>
    </Layout>
  );
};

export async function getServerSideProps(context: NextPageContext) {
  const { slug } = context.query;

  const response = await fetch(`http://localhost:3000/api/contests/${slug}`);

  const result = await response.json();

  return { props: { contest: result } };
}

export default Contest;
