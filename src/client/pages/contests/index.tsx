import { FC } from 'react';
import Link from 'next/link'
import fetch from 'isomorphic-fetch';
import Layout from '../../components/Layout';
import { IContest } from '../../../common';
import ContestsList from '../../components/ContestsList';

type ContestsProps = {
  drafts: IContest[];
}

const Contests: FC = ({ drafts }: ContestsProps) => (
  <Layout>
    <div className="container p-responsive-lr">
      <div className="flex flex-justify-between">
        <h1 className="title is-3">Contests</h1>
        <Link href="/contests/create/">
          <a href="/contests/create/" className="button is-primary">Create</a>
        </Link>
      </div>
      <ContestsList contests={drafts} />
    </div>
  </Layout>
);

export async function getServerSideProps() {
  const response = await fetch('http://localhost:3000/api/contests/');

  const result = await response.json() as IContest[];

  return {
    props: {
      drafts: result,
    },
  };
}

export default Contests;
