import { ChangeEvent, useState } from 'react';
import { NewContest } from '../../common';

enum ContestType {
  Plurality = 'Plurality',
  RankedSimple = 'RankedSimple',
  RankedEnhanced = 'RankedEnhanced',
  Condorcet = 'Condorcet',
  Approval = 'Approval',
}

export function useForm() {
  const [state, setState] = useState<NewContest>({
    type: ContestType.Plurality,
    title: '',
    choices: [],
    body: '',
    policy: '',
  });

  type FormElement = HTMLTextAreaElement | HTMLInputElement | HTMLSelectElement;

  const handleFormElementChange = (event: ChangeEvent<FormElement>) => {
    const { name, value } = event.currentTarget;

    setState((prevState) => ({
      ...prevState,
      [name]: name === 'choices' ? value.split('\n') : value,
    }));
  };

  return {
    form: state,
    onChange: handleFormElementChange,
  };
}
