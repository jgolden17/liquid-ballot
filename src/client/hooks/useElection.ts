import { ChangeEvent, SyntheticEvent, useState } from 'react';
import { gql } from '@apollo/client';
import { NewElection } from '../../common';
import client from '../apollo-client';

export function useElection() {
  const [election, setElection] = useState<NewElection>({
    title: '',
    notes: '',
    description: '',
    startDate: null,
    endDate: null,
  });

  const handleSubmit = async (e: SyntheticEvent) => {
    e.preventDefault();

    const { data, errors } = await client.mutate({
      mutation: gql`
        mutation createdElection($input: CreateElectionInput!) {
          createElection(input: $input) {
            __typename
            message
          }
        }
      `,
      variables: { input: { election } },
    })
  };

  type FormElement = HTMLTextAreaElement | HTMLInputElement | HTMLSelectElement;

  const handleFormElementChange = (event: ChangeEvent<FormElement>) => {
    const { name, value } = event.currentTarget;

    setElection((prevState) => ({
      ...prevState,
      [name]: value,
    }));
  };

  return {
    election,
    onChange: handleFormElementChange,
    onSubmit: handleSubmit,
  }
}
