import { SyntheticEvent } from 'react';
import { useForm } from '.';
import { ICreateContestDto } from '../../common';

export function useContestForm() {
  const { form, onChange } = useForm();

  const handleSubmit = async (e: SyntheticEvent) => {
    e.preventDefault();

    const payload: ICreateContestDto = {
      contest: form,
    };
  };

  return {
    form,
    onChange,
    onSubmit: handleSubmit,
  }
}
