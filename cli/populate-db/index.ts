import { CommandFactory } from 'nest-commander';
import { PopulateDatabaseModule } from './populate-db.module';

async function bootstrap() {
  await CommandFactory.run(PopulateDatabaseModule);
}

bootstrap();
