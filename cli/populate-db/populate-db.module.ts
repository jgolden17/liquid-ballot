import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { PopulateDatabase } from './populate-db.command';

@Module({
  imports: [MongooseModule.forRoot('mongodb://localhost:27017')],
  providers: [PopulateDatabase],
})
export class PopulateDatabaseModule {}
