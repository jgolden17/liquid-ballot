import { Command, CommandRunner } from 'nest-commander';
import { faker } from '@faker-js/faker';
import {
  ContestStatus,
  ContestType,
  ElectionStatus,
  IContest,
  IElection,
} from '../../src/common';
import { InjectConnection } from '@nestjs/mongoose';
import { createId } from '../../src/server/elections/utils';
import { Connection } from 'mongoose';

@Command({ name: 'populate-db', options: { isDefault: true } })
export class PopulateDatabase implements CommandRunner {
  constructor(@InjectConnection() private readonly connection: Connection) {}

  private createContests(count = 1): IContest[] {
    const contests: (IContest & { sequence: number })[] = [];

    for (let i = 0; i < count; i++) {
      const sequence = i + 1;

      const slug = createId('CONT', sequence);

      contests.push({
        sequence,
        type: ContestType.Plurality,
        title: faker.name.jobTitle(),
        body: faker.name.jobDescriptor(),
        policy: 'General',
        status: ContestStatus.Draft,
        slug,
        choices: [
          `${faker.name.firstName()} ${faker.name.lastName()}`,
          `${faker.name.firstName()} ${faker.name.lastName()}`,
          `${faker.name.firstName()} ${faker.name.lastName()}`,
          `${faker.name.firstName()} ${faker.name.lastName()}`,
        ],
      });
    }

    return contests;
  }

  private createElections(): Omit<IElection, 'id'>[] {
    const elections: (Omit<IElection, 'id'> & { sequence: number })[] = [];

    let contests: IContest[] = [];

    for (let i = 0; i < 20; i++) {
      const startDate = faker.date.soon();

      const title = `${faker.address.city()} Branch Election`;

      const sequence = i + 1;

      const slug = createId('ELEC', sequence);

      const contests_ = this.createContests(3);

      contests = [...contests, ...contests_];

      elections.push({
        sequence,
        title,
        notes: faker.random.words(),
        description: `${title} \n\n ${faker.random.words()}`,
        startDate,
        endDate: faker.date.future(1, startDate),
        slug,
        status: ElectionStatus.Draft,
        contests: contests_,
        createdAt: faker.date.past(1, startDate),
      });
    }

    return elections;
  }

  async run(): Promise<void> {
    const elections = this.createElections();

    await this.connection.collection('elections').insertMany(elections);
  }
}
